import Controllers from "./Components/Controllers/Controllers.jsx";
import Button from "./Components/Controllers/Button.jsx";
import ModalImage from "./Components/Modal/ModalBasic.jsx";
import ModalText from "./Components/Modal/ModalBasic.jsx";
import ModalWrapper from "./Components/Modal/ModalWrapper.jsx";
import Modal from "./Components/Modal/Modal.jsx";
import ModalHeader from "./Components/Modal/ModalHeader.jsx";
import ModalClose from "./Components/Modal/ModalClose.jsx";
import ModalBody from "./Components/Modal/ModalBody.jsx";
import ModalFooter from "./Components/Modal/ModalFooter.jsx";
import {useState} from "react";

function App() {

    const [isOpenFirst, setIsOpenFirst] = useState(false);

    const actionFirst = () => {
        setIsOpenFirst(!isOpenFirst)
    }
    const [isOpenSecond, setIsOpenSecond] = useState(false);

    const actionSecond = () => {
        setIsOpenSecond(!isOpenSecond)
    }
    return (
        <>
            <Controllers>
                <Button classNames='btn btn-first' click={actionFirst}>Open first modal</Button>
                <Button classNames='btn btn-second' click={actionSecond}>Open second modal</Button>
            </Controllers>
            {isOpenFirst && (
                <ModalImage>
                    <ModalWrapper click={actionFirst}>
                        <Modal>
                            <ModalHeader>
                                <ModalClose click={actionFirst}/>
                            </ModalHeader>
                            <ModalBody
                                img={<div className='modal-img'></div>}
                                title='Product Delete!'
                                desc='By clicking the “Yes, Delete” button, PRODUCT NAME will be
                                deleted.'>
                            </ModalBody>
                            <ModalFooter>
                                <Button classNames="modal-btn">NO, CANCEL</Button>
                                <Button classNames="modal-btn-active">YES, DELETE</Button>
                            </ModalFooter>
                        </Modal>
                    </ModalWrapper>
                </ModalImage>)}
            {isOpenSecond && (
                <ModalText>
                    <ModalWrapper click={actionSecond}>
                        <Modal>
                            <ModalHeader>
                                <ModalClose click={actionSecond}/>
                            </ModalHeader>
                            <ModalBody
                                title='Add Product “NAME”'
                                desc='escription for your product'>
                            </ModalBody>
                            <ModalFooter>
                                <Button classNames="modal-btn-active">ADD TO FAVORITE</Button>
                            </ModalFooter>
                        </Modal>
                    </ModalWrapper>
                </ModalText>
            )}
        </>
    )
}

export default App
