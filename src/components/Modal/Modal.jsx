import PropTypes from "prop-types";

const Modal = ({children}) => {
    const stopClick = (event) => {
        event.stopPropagation()
    }
    return (
        <div onClick={stopClick} className="modal">
            {children}
        </div>
    );
};

export default Modal;

Modal.propTypes = {
    children: PropTypes.any,
}