import PropTypes from "prop-types";

const ModalBody = ({img, title, desc}) => {
    return (
        <div className="modal-body">
            {img}
            <h2 className='modal-title'>{title}</h2>
            <p className='modal-txt'>{desc}</p>
        </div>
    );
};

export default ModalBody;

ModalBody.propTypes = {
    img: PropTypes.any,
    title: PropTypes.string,
    desc: PropTypes.string,
}