import PropTypes from "prop-types";
import "./Modal.scss"

const ModalBasic = ({children}) => {
    return (
        <div>
            {children}
        </div>
    );
};

export default ModalBasic;

ModalBasic.propTypes = {
    children: PropTypes.any,
}