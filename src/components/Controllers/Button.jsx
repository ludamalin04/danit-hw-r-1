import PropTypes from "prop-types";

const Button = (props) => {
    const {type, classNames, click, children} = props
    return (
        <>
            <button onClick={click} className={classNames} type={type}>{children}</button>
        </>
    )
}
export default Button

Button.defaultProps = {
    type: "button",
}
Button.propTypes = {
    type: PropTypes.string,
    classNames: PropTypes.string,
    click: PropTypes.func,
    children: PropTypes.any
}